//
//  MeMotiverApp.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 18/09/2020.
//

import SwiftUI

@main
struct MeMotiverApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
