//
//  ContentView.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 18/09/2020.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        // 1
        TabView {
            // 2
            HomeView()
                // 3
                .tabItem {
                    VStack {
                        Image(systemName: "house.fill")
                        Text("Accueil")
                    }
            // 4
            }.tag(1)
            
            // 5
            HabitView()
                .tabItem {
                    VStack {
                        Image(systemName: "list.dash")
                        Text("Habitudes")
                    }
            }.tag(2)
            // 5
            ActivitiesView()
                .tabItem {
                    VStack {
                        Image(systemName: "calendar")
                        Text("Activité")
                    }
            }.tag(2)
            
            ProgressView()
                // 3
                .tabItem {
                    VStack {
                        Image(systemName: "checkmark.seal.fill")
                        Text("Progrès")
                    }
            // 4
            }.tag(3)
            
            ParamatersView()
                // 3
                .tabItem {
                    VStack {
                        Image(systemName: "gear")
                        Text("Paramètres")
                    }
            // 4
            }.tag(4)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
