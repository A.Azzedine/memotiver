//
//  HabitViewModel.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 08/05/2022.
//

import Foundation

class HabitViewModel: ObservableObject {
    
    @Published private(set) var habits : [Habit] = [Habit(name: "Aller à la salle",nextOccurence: "Prochaine activité jeudi",iconName: "habit_sport_icon", colorValue: "sport_color"),Habit(name: "Miracle morning",nextOccurence: "Prochaine activité demain",iconName: "habit_self_icon", colorValue: "self_color"),Habit(name: "Boire de l'eau",nextOccurence: "Prochaine activité à 15h",iconName: "habit_health_icon", colorValue: "health_color"),Habit(name: "Apprendre à dessiner",nextOccurence: "Prochaine activité vendredi",iconName: "habit_art_icon", colorValue: "art_color"),Habit(name: "Jouer du panio",nextOccurence: "Prochaine activité à 19h",iconName: "habit_art_icon", colorValue: "art_color")]
    
    
    func addRandomHabit(){
        habits.append(habits[0]);
    }
    
    public func generateNewEmptyHabit() -> Habit{
        return Habit();
    }
    
}
