//
//  HabitCardView.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 21/09/2020.
//

import SwiftUI

struct HabitCardView: View {
    
    var habit: Habit
    
    init(habit: Habit) {
        self.habit = habit
    }
    var body: some View {
            VStack(alignment: .trailing) {
                VStack(alignment:.trailing){
                    HStack(){
                        Text(self.habit.name)
                            .font(.title2)
                            .fontWeight(.black)
                            .foregroundColor(.white)
                            .multilineTextAlignment(.leading)
                            .lineLimit(2)
                            .frame(alignment:.topLeading)
                        Spacer()
                        Image(habit.iconName)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                            .frame(alignment:.bottomTrailing)
                    }
                
               
                    
                    Spacer()
                    Text(self.habit.nextOccurence)
                        .font(.caption)
                        .foregroundColor(.white)
                        .multilineTextAlignment(.trailing)
                        .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
                        .frame(alignment:.bottomTrailing)
                }
            }
            .frame(minHeight:100,maxHeight: 100,alignment: .topTrailing)
        .padding()
            .background(Color(habit.colorValue))
        .cornerRadius(10)
        .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.3), lineWidth: 2)
        )
               
    }
}

struct HabitCardView_Previews: PreviewProvider {
    static var previews: some View {
        HabitCardView(habit: Habit(name: "Aller à la salle",nextOccurence: "Prochaine activité demain",iconName: "habit_sport_icon", colorValue: "sport_color"))
            .padding()
    }
}
