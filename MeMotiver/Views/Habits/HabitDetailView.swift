//
//  HabitDetailView.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 27/09/2020.
//

import SwiftUI

struct HabitDetailView: View {
    
    var habit: Habit
    
    init(habit: Habit) {
        self.habit = habit
    }
    init(){
        self.habit = Habit()
    }
    
    var body: some View {
        HStack{
            ScrollView(.vertical) {
                VStack(alignment: .leading){
                    Image(habit.iconName)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(alignment:.center)
                }
                .frame( height: 80)
                .padding(.bottom)
                
                Divider().background(Color.black)
                
                Text(habit.name)
                    .font(.title2)
                    .fontWeight(.black)
                    .lineLimit(2)
                
                Divider().background(Color.black)
                VStack(alignment: .leading){
                    Text("Fréquence:")
                        .font(.headline)
                        .multilineTextAlignment(.leading)
                    Text("Tous les jours")
                        .font(.caption)
                }
                .frame(width: .infinity,  alignment: .leading)
                
            }.padding([.vertical,.horizontal])
            Spacer()
        }
        .background(Color(habit.colorValue))
        .cornerRadius(radius: 40, corners: [.topLeft, .topRight])
        .padding([.horizontal])
            
    }
}

struct HabitDetailView_Previews: PreviewProvider {
    static var previews: some View {
        HabitDetailView(habit: Habit(name: "Aller à la salle",nextOccurence: "Prochaine activité demain",iconName: "habit_sport_icon", colorValue: "sport_color"))
    }
}
