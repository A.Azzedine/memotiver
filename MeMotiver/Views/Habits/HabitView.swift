//
//  HabitView.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 19/09/2020.
//

import SwiftUI

struct HabitView: View {
        
    @StateObject private var habitsViewModel = HabitViewModel()
    @StateObject private var emptyHabit = Habit()
    @State var isLinkActive = false
    
    var body: some View {
        NavigationView{
            ZStack{
            
                HStack{
                    ScrollView(.vertical) {
                    VStack{
                        ForEach(habitsViewModel.habits, id: \.self){ habit in
                            NavigationLink(destination: HabitDetailView(habit: habit)){
                                HabitCardView(habit: habit)

                            }
                        }
                        Spacer()
                    }.padding([.top,.horizontal])
                
                    }
                }
                VStack {
                                Spacer()
                                HStack {
                                    Spacer()
                                    NavigationLink(destination: HabitDetailView(), isActive: $isLinkActive) {
                                    Button(action: {
                                        self.isLinkActive = true
                                    }, label: {
                                        Text("+")
                                        .font(.system(.largeTitle))
                                        .frame(width: 77, height: 70)
                                        .foregroundColor(Color.white)
                                        .padding(.bottom, 7)
                                    })
                                    }
                                    
                                    .background(Color.blue)
                                    .cornerRadius(38.5)
                                    .padding()
                                    .shadow(color: Color.black.opacity(0.3),
                                            radius: 3,
                                            x: 3,
                                            y: 3)
                                }
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .navigationBarTitle("Mes habitudes")
        }
        
    }
    
    /**func redirectToNewHabitForm(){
        let habitToCreate = habitsViewModel.generateNewEmptyHabit()
        NavigationLink("Ajout d'une habitude", destination: HabitDetailView(habit: habitToCreate))
    
    }**/
}

struct HabitView_Previews: PreviewProvider {
    static var previews: some View {
        HabitView()
    }
}


