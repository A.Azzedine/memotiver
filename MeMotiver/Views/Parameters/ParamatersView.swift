//
//  ParamatersView.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 21/09/2020.
//

import SwiftUI

struct ParamatersView: View {
    var body: some View {
        ZStack {
            Text("Parameter View")
                .font(.largeTitle)
        }
    }
}

struct ParamatersView_Previews: PreviewProvider {
    static var previews: some View {
        ParamatersView()
    }
}
