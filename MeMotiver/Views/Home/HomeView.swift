//
//  HomeView.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 19/09/2020.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        ZStack {
            Text("Home View")
                .font(.largeTitle)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
