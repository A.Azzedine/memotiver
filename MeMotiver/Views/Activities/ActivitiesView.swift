//
//  SwiftUIActivitiesView.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 24/09/2020.
//

import SwiftUI

struct ActivitiesView: View {
    var body: some View {
        Text("Activities view")
    }
}

struct SwiftUIActivitiesView_Previews: PreviewProvider {
    static var previews: some View {
        ActivitiesView()
    }
}
