//
//  ProgressView.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 21/09/2020.
//

import SwiftUI

struct ProgressView: View {
    var body: some View {
        ZStack {
            Text("Progress View")
                .font(.largeTitle)
        }
    }
}

struct ProgressView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressView()
    }
}
