//
//  Habit.swift
//  MeMotiver
//
//  Created by Azzedine AIT ELHAJ on 21/09/2020.
//

import Foundation

class Habit: Identifiable, Hashable, Codable, ObservableObject{
    static func == (lhs: Habit, rhs: Habit) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
    }

    
    var id = UUID()
    var name: String
    var nextOccurence: String
    var iconName: String
    var colorValue: String
    
    init(name: String, nextOccurence: String, iconName: String, colorValue: String) {
        self.name = name
        self.nextOccurence = nextOccurence
        self.iconName = iconName
        self.colorValue = colorValue
    }
    
    init(){
        self.name = ""
        self.nextOccurence = ""
        self.iconName = ""
        self.colorValue = ""
        
    }
    
}
